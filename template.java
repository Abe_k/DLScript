package downloader;

import java.io.*;
import java.net.*;
import java.util.*;

import org.openqa.selenium.*;
import org.openqa.selenium.phantomjs.*;
import org.openqa.selenium.remote.DesiredCapabilities;

public abstract class Downloader {

    protected static WebDriver driver;

    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    private static JavascriptExecutor js = (JavascriptExecutor) driver;

    private static byte[] packet = new byte[1024];

    protected static String readLine() {
        String buffer = "";
        try {
            buffer = br.readLine();
        } catch (IOException e) {
            // Pass
        }

        return buffer;
    }

    protected static void saveScreenshot(String filename) {
        File fin = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File fout = new File(filename);

        try {
            FileInputStream fis = new FileInputStream(fin);
            FileOutputStream fos = new FileOutputStream(fout);

            try {
                int length = 0;

                while ((length = fis.read(packet)) != -1) {
                    fos.write(packet, 0, length);
                }

            } catch (Exception e) {
                throw e;
            } finally {
                if (fis != null)
                    fis.close();
                if (fos != null)
                    fos.close();
            }

        } catch (Exception e) {
            // Pass
        }
    }

    protected static List<WebElement> getListElements(String containerXpath, String listXpath) {
        WebElement container = driver.findElement(By.xpath(containerXpath));
        return container.findElements(By.xpath(listXpath));
    }

    protected static void showElementsList(List<WebElement> elems) {
        for (int i = 0; i < elems.size(); i += 1) {

            try {
                System.out.println(i + " : " + elems.get(i).getText());
            } catch (Exception e) {
                System.out.println("No text is found in the elemsnt");
            }

        }
    }

    protected static boolean submitButton(String buttonXpath) {
        try {
            driver.findElement(By.xpath(buttonXpath)).click();
            return true;

        } catch (Exception e) {
            return false;

        }
    }

    protected static void executeJavaScript(String script) {
        js.executeScript(script);
    }

    protected static boolean download(String src, String filename) {
        int length = 0;

        try {
            URL url = new URL(src);
            URLConnection conn = url.openConnection();
            InputStream in = conn.getInputStream();

            File file = new File(filename);
            FileOutputStream out = new FileOutputStream(file, false);

            while ((length = in.read(packet)) != -1) {
                out.write(packet, 0, length);
            }

            in.close();
            out.close();

            return true;

        } catch (Exception e) {
            return false;

        }
    }

    protected static void setupGhostDriver(String phantomjsPath) {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(
            PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
            phantomjsPath
        );

        driver = new PhantomJSDriver(caps);
    }

}
