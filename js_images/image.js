(function() {
    var extension = {
        jpg: [
            /\.jpg$/,
            /\.JPG$/,
            /\.jpeg/
        ],
        png: [
            /\.png$/,
            /\.PNG$/
        ],
        gif: [
            /\.gif$/,
            /\.GIF$/
        ],
        bmp: [
            /\.bmp$/,
            /\.BMP$/
        ]
    };

    var imglist = [];

    var isObject = function(target) {
        return target && typeof target === 'object';
    };
    var isArray = function(target) {
        return isObject(target) && target.constructor === Array;
    };
    var isString = function(target) {
        return typeof target === 'string';
    };

    var addSimpleElem = function(args) {
        var parent = isObject(args.parent) ? args.parent : document.body;
        var tagName = isString(args.tagName) ? args.tagName : "div";

        var elem = document.createElement(tagName);
        if (isString(args.name)) {
            elem.setAttribute("name", args.name);
        }

        parent.appendChild(elem);
        return elem;
    };

    var addListElem = function(args) {
        var parent = isObject(args.parent) ? args.parent : document.body;
        var list = isArray(args.list) ? args.list : [];
        var container = addSimpleElem({
            parent: parent,
            tagName: "ul"
        });

        for (var i = 0; i < list.length; i += 1) {
            var img = addSimpleElem({
                parent: container,
                tagName: "li"
            });
            img.innerText = list[i];
        }

        return container;
    };

    var addCheckbox = function(args) {
        var checkbox = addSimpleElem({
            parent: args.parent,
            tagName: "input",
            name: args.name
        });
        checkbox.setAttribute("type", "checkbox");

        if (args.checked === true) {
            checkbox.checked = true;
        }
        if (isString(args.name)) {
            checkbox.parentNode.appendChild(document.createTextNode(args.name));
        }

        return checkbox;
    };

    var addButton = function(args) {
        var name = isString(args.name) ? args.name : "button";
        var button = addSimpleElem({
            parent: args.parent,
            tagName: "input",
            name: name
        });

        button.setAttribute("type", "button");
        button.setAttribute("value", name);

        return button;
    };

    var removeAllElem = function() {
        document.open();
        document.close();
    };

    var onExtract = function(e) {
        var extracted = [];
        var regs = [];
        var filters = document.getElementById("filters").getElementsByTagName("input");

        for (var i = 0; i < filters.length; i += 1) {
            if (filters[i].checked) {
                var exts = isArray(extension[filters[i].id]) ? extension[filters[i].id] : [];

                for (var j = 0; j < exts.length; j += 1) {
                    regs.push(exts[j]);
                }
            }
        }

        for (var i = 0; i < regs.length; i += 1) {
            for (var j = 0; j < imglist.length; j += 1) {
                if (regs[i].test(imglist[j])) {
                    extracted.push(imglist[j]);
                }
            }
        }

        var container = document.getElementById("imglist").parentNode;
        container.removeChild(document.getElementById("imglist"));
        addListElem({
            parent: container,
            list: extracted
        }).setAttribute("id", "imglist");
    };

    // Get img elements src
    var elems = document.getElementsByTagName("img");
    for (var i = 0; i < elems.length; i += 1) {
        if (isString(elems[i].src)) {
            imglist.push(elems[i].src);
        }
    }

    removeAllElem();

    var header = addSimpleElem({
        parent: document.body,
        tagName: "div",
        name: "header"
    });
    var main = addSimpleElem({
        parent: document.body,
        tagName: "div",
        name: "main"
    });

    addListElem({
        parent: main,
        list: imglist
    }).setAttribute("id", "imglist");

    var filters = addSimpleElem({
        parent: header,
        tagName: "form",
        name: "filters"
    });
    filters.setAttribute("id", "filters");
    var buttons = addSimpleElem({
        parent: header,
        tagName: "form",
        name: "buttons"
    });

    var jpg = addCheckbox({
        parent: filters,
        name: "JPG images",
        checked: true
    });
    jpg.setAttribute("id", "jpg");
    var png = addCheckbox({
        parent: filters,
        name: "PNG images",
        checked: true
    });
    png.setAttribute("id", "png");
    var gif = addCheckbox({
        parent: filters,
        name: "GIF images",
        checked: true
    });
    gif.setAttribute("id", "gif");
    var bmp = addCheckbox({
        parent: filters,
        name: "BMP images"
    });
    bmp.setAttribute("id", "bmp");

    var extract = addButton({
        parent: buttons,
        name: "extract"
    });
    extract.addEventListener("click", onExtract, false);
    var download = addButton({
        parent: buttons,
        name: "download"
    });
})();
