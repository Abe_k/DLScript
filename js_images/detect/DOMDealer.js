var DOMDealer = function() {
    var method = {};

    // Identify variable type
    var isObject = function(target) {
        return target && typeof target === 'object';
    };
    var isArray = function(target) {
        return isObject(target) && target.constructor === Array;
    };
    var isElement = function(target) {
        try {
            return target instanceof HTMLElement;
        } catch (e) {
            return (
                typeof target === 'object' &&
                target.nodeType === 1 &&
                typeof target.style === 'object' &&
                typeof target.ownerDocument === 'object'
            );
        }
    };
    var isString = function(target) {
        return typeof target === 'string';
    };
    var isNumber = function(target) {
        return typeof target === 'number';
    };

    // Add new element
    method.addElement = function(args) {
        if (!isElement(args.parent)) {
            return;
        }

        var parent = args.parent;
        var tagName = isString(args.tagName) ? args.tagName : "div";

        var elem = document.createElement(tagName);

        if (isString(args.name)) {
            elem.setAttribute("name", args.name);
        }
        if (isString(args.id)) {
            elem.setAttribute("id", args.id);
        }

        parent.appendChild(elem);
        return elem;
    };

    // Replace element
    method.replaceElement = function(args) {
        if (!isElement(args.old)) {
            return;
        }
        if (!isElement(args.old.parentNode)) {
            return;
        }

        var parent = args.old.parentNode;
        var tagName = isString(args.tagName) ? args.tagName : args.old.localName;

        var elem = document.createElement(tagName);

        if (isString(args.name)) {
            elem.setAttribute("name", args.name);
        }
        if (isString(args.id)) {
            elem.setAttribute("id", args.id);
        }

        parent.replaceChild(elem, args.old);
        return elem;
    };

    // List element
    var constructListElement = function(args) {
        var list = isArray(args.list) ? args.list : [];

        for (var i = 0; i < list.length; i += 1) {
            var img = method.addElement({
                parent: args.parent,
                tagName: "li"
            });

            if (isElement(img)) {
                img.innerText = list[i];
            }
        }

        return args.parent;
    };
    method.addListElement = function(args) {
        var container = method.addElement({
            parent: args.parent,
            tagName: "ul",
            name: args.name,
            id: args.id
        });

        return constructListElement({
            parent: container,
            list: args.list
        });
    };
    method.replaceListElement = function(args) {
        var container = method.replaceElement({
            old: args.old,
            tagName: "ul",
            name: args.name,
            id: args.id
        });

        return constructListElement({
            parent: container,
            list: args.list
        });
    };

    // Table element
    var constructTableElement = function(args) {
        if (!isElement(args.parent)) {
            return;
        }

        var parent = args.parent;
        var list = isArray(args.list) ? args.list: [];
        var aspect = isArray(args.aspect) ? args.aspect: [];
        var border = isNumber(args.border) ? args.border: 1;

        parent.setAttribute("border", border);

        for (var i = 0; i < list.length; i += 1) {
            if (!isArray(list[i])) {
                list[i] = [list[i]];
            }

            for (var j = 0; j < list[i].length; j += 1) {
                var elem = method.addElement({
                    parent: parent,
                    tagName: "td"
                });
                elem.innerText = list[i][j];

                if (isNumber(aspect[j])) {
                    elem.setAttribute("width", aspect[j]);
                }
            }

            method.addElement({
                parent: parent,
                tagName: "tr"
            });
        }

        return parent;
    };
    method.addTableElement = function(args) {
        var container = method.addElement({
            parent: args.parent,
            tagName: "table",
            name: args.name,
            id: args.id
        });

        return constructTableElement({
            parent: container,
            list: args.list,
            aspect: args.aspect,
            border: args.border
        });
    };
    method.replaceTableElement = function(args) {
        var container = method.replaceElement({
            old: args.old,
            tagName: "table",
            name: args.name,
            id: args.id
        });

        return constructTableElement({
            parent: container,
            list: args.list,
            aspect: args.aspect,
            border: args.border
        });
    };

    // Checkbox element
    var constructCheckbox = function(args) {
        if (!isElement(args.parent)) {
            return;
        }

        var checkbox = method.addElement({
            parent: args.parent,
            tagName: "input",
            name: args.name,
            id: args.id
        });

        checkbox.setAttribute("type", "checkbox");

        if (args.checked === true) {
            checkbox.checked = true;
        }
        if (isString(args.label)) {
            args.parent.appendChild(document.createTextNode(args.label));
        }

        return args.parent;
    };
    method.addCheckbox = function(args) {
        var container = method.addElement({
            parent: args.parent,
            tagName: "div"
        });

        return constructCheckbox({
            parent: container,
            name: args.name,
            id: args.id,
            checked: args.checked,
            label: args.label
        });
    };
    method.replaceCheckbox = function(args) {
        var container = method.replaceElement({
            old: args.old,
            tagName: "div"
        });

        return constructCheckbox({
            parent: container,
            name: args.name,
            id: args.id,
            checked: args.checked,
            label: args.label
        });
    };

    // Button element
    var createButton = function(args) {
        if (!isElement(args.button)) {
            return;
        }

        var button = args.button;
        var label = isString(args.label) ? args.label : "";

        button.setAttribute("type", "button");
        button.setAttribute("value", label);

        return button;
    };
    method.addButton = function(args) {
        var button = method.addElement({
            parent: args.parent,
            tagName: "input",
            name: args.name,
            id: args.id
        });

        return createButton({
            button: button,
            label: args.label
        });
    };
    method.replaceButton = function(args) {
        var button = method.replaceElement({
            old: args.old,
            tagName: "input",
            name: args.name,
            id: args.id
        });

        return createButton({
            button: button,
            label: args.label
        });
    };

    return method;
};
