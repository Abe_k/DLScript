#!/bin/bash

file='config.ini'

echo "[locale_setting]" > $file
while read line
do
    echo $line >> $file
done << EOS
save_directory = 

[remote_settings]
download_URL = 

[filename_format]
name = 
alter_charactor = ?
divider = |

# buffered charactor
buffer_1 = 0
buffer_2 = 0
buffer_3 = 0

# initial number
init_1 = 1
init_2 = 1
init_3 = 1

[default_setting]
default_buffer = 0
default_init = 1
EOS
