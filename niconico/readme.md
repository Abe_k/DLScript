Nicoseiga.py
============

"Nicoseiga.py" is a python script which allows you to download free comics published in "[Niconico-seiga](http://seiga.nicovideo.jp/)".
You can download comics in two ways. One is to choose from the latest comic list. The other is the use of search engine.

Requirements
------------

* python v2.7.x
* Selenium v2.53.6
* PhantomJS v2.1.1

References:
* [Selenium with Python](http://selenium-python.readthedocs.io/index.html)
* [Selenium API](http://seleniumhq.github.io/selenium/docs/api/py/api.html)
* [PhantomJS](http://phantomjs.org)


Usage
-----

1. Write your niconico-video account data to [config.ini](https://gitlab.com/Abe_k/DLScript/blob/master/niconico/config.ini). This data will use to login niconico account.
2. Execute "Nicoseiga.py" on terminal. No argument is required.
3. Follow the instruction to choose the topics.
4. Automatically download JPG images to locale.

Notes:
* If you has logged in niconico-video account wrriten in config.ini on other apps such as browzer and smartphone app, this script sometimes cannot login niconico-video.
