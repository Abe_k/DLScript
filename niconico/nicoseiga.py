#!/usr/bin/env python2

from selenium import webdriver
import urllib
import os
import time

# Set driver
driver = webdriver.PhantomJS()
driver.get("http://seiga.nicovideo.jp/manga/")


def login():
    try:
        print("go to login page.")
        # Get login button element
        elem = driver.find_element_by_id('siteHeaderNotification')
        # Move to login page
        elem = elem.find_element_by_xpath('.//a')
        url = elem.get_attribute("href")
        driver.get(url)
    except:
        # Already logged in
        print("you has already logged in.")
        return

    # Get login form elements
    mail = driver.find_element_by_id("input__mailtel")
    password = driver.find_element_by_id("input__password")

    # Complete login form
    import ConfigParser
    config = ConfigParser.SafeConfigParser()
    config.read('config.ini')
    mail.send_keys(config.get('login', 'email'))
    password.send_keys(config.get('login', 'password'))
    print("config.ini has loaded.")

    # Submit form
    driver.find_element_by_id("login__submit").click()


def logout():
    try:
        driver.get("http://seiga.nicovideo.jp/logout")
    except:
        print("cannot logout.")


def getCategoryElements():
    # Get category list elements
    container = driver.find_element_by_id("mg_category_list")
    elems = container.find_elements_by_xpath('.//ul/li')

    # Get each link elements
    for i in range(len(elems)):
        elems[i] = elems[i].find_element_by_xpath(".//a")

    return elems


def getContentElements():
    # Get content list elements
    elems = driver.find_elements_by_xpath('//div[@id="comic_list"]/ul/li')

    # Get each title element
    for i in range(len(elems)):
        elems[i] = elems[i].find_element_by_xpath('.//div[@class="title"]/a')

    return elems


def getEpisodeElements():
    # Get episode list elements
    elems = driver.find_elements_by_xpath('//div[@id="episode_list"]/ul/li')

    # Get each link element
    for i in range(len(elems)):
        elems[i] = elems[i].find_element_by_xpath('.//div[@class="title"]/a')

    return elems


def printCategoryMenu(elems):
    size = len(elems)
    for i in range(len(elems)):
        print(str(i) + " : " + elems[i].text)

    print("\ninput the number to choose category (0 - " + str(size - 1) + ")")
    print("or press \'s\' key to switch search comics mode.")

    # Return input key
    return raw_input(">>")


def printContentMenuWithList(elems):
    size = len(elems)

    # Print content list
    for i in range(size):
        print(str(i) + " : " + elems[i].text)

    print("\ninput the number to choose content (0 - " + str(size - 1) + ")")
    print("press \'l\' key to show next page.")
    print("press \'h\' key to show previous page.")

    # Return input key
    return raw_input(">>")


def printEpisodeMenu(elems):
    size = len(elems)

    print("\ninput initial download episode (1 - " + str(size) + ")")

    try:
        idx = int(raw_input("(default : 1) >>")) - 1
        if idx < 0 or not idx < size:
            idx = 0
    except:
        idx = 0
    # Return input key
    return idx


def goNextPage():
    try:
        # Get pager button element
        elem = driver.find_element_by_xpath('//a[@rel="next"]')
        elem.click()
    except:
        print("This is the last page.")


def goPrevPage():
    try:
        # Get pager button element
        elem = driver.find_element_by_xpath('//a[@rel="prev"]')
        elem.click()
    except:
        print("This is the first page.")


def goNextEpisode():
    try:
        elem = driver.find_element_by_xpath('//a[@class="next"]')
        elem.click()
        return True
    except:
        # Next button not found
        return False


def acceptCategoryOperation():
    # Get category list
    elems = getCategoryElements()

    while True:
        cmd = printCategoryMenu(elems)
        try:
            idx = int(cmd)
            elems[idx].click()
            return
        except:
            if cmd == 's':
                acceptContentOperationWithSearh()
                break


def acceptContentOperationWithList(dst_dir=""):
    # Get comic list
    elems = getContentElements()

    # Accept operation from user
    while True:
        cmd = printContentMenuWithList(elems)
        try:
            idx = int(cmd)
            dst_dir = dst_dir + elems[idx].text
            elems[idx].click()
            break
        except:
            if cmd == 'l':
                goNextPage()
                elems = getContentElements()
            elif cmd == 'h':
                goPrevPage()
                elems = getContentElements()

    return dst_dir


def acceptContentOperationWithSearh():
    # Get search form
    search = driver.find_element_by_id("bar_search")

    # Input search word
    print("input search word")
    word = raw_input(">>").decode('utf-8')
    search.send_keys(word)

    driver.find_element_by_id("search_button").click()


def downloadImagesOnPage(dirpath, idx):
    # Get image list
    container = driver.find_element_by_id("page_contents")
    elems = container.find_elements_by_xpath(".//li")

    for i in range(len(elems)):
        url = elems[i].find_element_by_xpath(".//img[1]").get_attribute("src")
        filename = dirpath + "/" + str(idx) + "-" + str(i) + ".jpg"

        try:
            print("download " + filename)
            img = urllib.urlopen(url)
            localefile = open(filename, 'wb')
            localefile.write(img.read())
            img.close()
            localefile.close()
            driver.execute_script("window.scrollTo(0, " + str(1000 * i) + ")")
            time.sleep(1.0)
        except:
            print("can't download " + url)


def downloadImagesWithList(dst_dir):
    # Load episode elements
    elems = getEpisodeElements()

    # Get initial download index
    idx = printEpisodeMenu(elems)
    elems[idx].click()

    # Generate save directory
    if not os.path.exists(dst_dir):
        os.mkdir(dst_dir)

    while True:
        downloadImagesOnPage(dst_dir, idx + 1)
        idx += 1
        if not goNextEpisode():
            break


if __name__ == '__main__':
    print("connect to " + driver.title)
    login()

    acceptCategoryOperation()

    dst_dir = acceptContentOperationWithList()

    downloadImagesWithList(dst_dir)

    logout()
    driver.close()
