#!/usr/bin/env python2
# -*- coding: utf-8 -+-

import os
import sys
import ConfigParser
import urllib

config_file = 'config.ini'
if len(sys.argv) > 1:
    config_file = sys.argv[1]

config = ConfigParser.SafeConfigParser()
config.read(config_file)

DST = config.get('locale_settings', 'save_directory')
SRC = config.get('remote_settings', 'download_URL')
ALT = config.get('filename_format', 'alter_charactor')
DIV = config.get('filename_format', 'divider')
NAME = config.get('filename_format', 'name')
DEFAULT = config.get('default_setting', 'default_buffer')
INI = config.get('default_setting', 'default_init')


def match(string, char):
    for c in string:
        if c == char:
            return True
    return False


def convertName(name):
    buf = ""
    idx = 0
    count = 0
    extra = list()
    digit = list()
    for i in range(len(name)):
        char = name[i]

        if char == ALT:
            if not match(buf, ALT):
                if buf == DIV:  # ignore divider
                    buf = ""
                extra.append(buf)
                buf = ""

            buf = buf + char
            count += 1

        else:
            if count != 0:
                digit.append(count)
                buf = char
                count = 0
            else:
                buf = buf + char

    if count == 0:
        extra.append(buf)
    else:
        digit.append(count)
        extra.append("")

    return {
        "extra": extra,
        "digit": digit
    }


def downloadManager(level, stack=""):
    flag = 0
    idx = FORMAT['begin'][level]
    digit = FORMAT['digit'][level]
    buffer = FORMAT['buffer'][level]
    extra = FORMAT['extra'][level]
    last = len(FORMAT['begin']) - 1

    while flag == 0:
        if buffer == "":
            number = str(idx)
        else:
            number = str(idx).rjust(digit, buffer)

        if level == last:
            filename = stack + extra + number + FORMAT['extra'][level + 1]
            print(SRC + filename + ' -> '),
            print(DST + '\\' + filename)
            pwd = os.path.abspath(os.path.dirname(__file__)) + '\\'
            cmd = 'bitsadmin.exe /TRANSFER download '
            os.system(cmd + SRC + filename + ' ' + pwd + DST + '\\' + filename)
        else:
            downloadManager(level + 1, stack + extra + number)
        idx += 1



if len(ALT) != 1:
    ALT = ALT[0]
if len(DIV) != 1:
    DIV = DIV[0]
if len(DEFAULT) != 1:
    DEFAULT = DEFAULT[0]

FORMAT = convertName(NAME)

print(FORMAT)
buf = list()
begin = list()
for i in range(len(FORMAT['digit'])):
    try:
        temp = config.get('filename_format', 'buffer_' + str(i + 1))
        buf.append(temp)
    except:
        buf.append(DEFAULT)

    try:
        temp = int(config.get('filename_format', 'init_' + str(i + 1)))
        begin.append(temp)
    except:
        begin.append(INI)

FORMAT['buffer'] = buf
FORMAT['begin'] = begin


if __name__ == '__main__':
    pwd = os.path.abspath(os.path.dirname(__file__)) + '\\'
    if not os.path.exists(pwd + DST):
        os.mkdir(pwd + DST)
    downloadManager(0)
