@echo off
cd /d %~dp0

set dir=%~dp0
set url1=https://www.python.org/ftp/python/2.7.12/
set file1=python-2.7.12.msi

set url2=https://gitlab.com/Abe_k/DLScript/raw/master/
set file2=download_win.py
set file3=make.sh
set file4=sample.ini

bitsadmin.exe /TRANSFER download %url1%%file1% %dir%%file1%
bitsadmin.exe /TRANSFER download %url2%%file2% %dir%%file2%
bitsadmin.exe /TRANSFER download %url2%%file3% %dir%%file3%
bitsadmin.exe /TRANSFER download %url2%%file4% %dir%%file4%

%file1%
%file2% %file4%